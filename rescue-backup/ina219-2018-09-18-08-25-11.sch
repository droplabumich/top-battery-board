EESchema Schematic File Version 2
LIBS:Top_Battery_Plate-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:Sphere_lib
LIBS:Top_Battery_Plate-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 5
Title "Power Management Board"
Date "2017-10-10"
Rev "0.4"
Comp "DROP Lab, University of Michigan"
Comment1 "Designed by Eduardo Iscar"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L ina219_SOT23 U4
U 1 1 57B867BF
P 4900 3750
F 0 "U4" H 4900 3650 60  0000 C CNN
F 1 "ina219_SOT23" H 4950 4250 60  0000 C CNN
F 2 "Sphere_lib_footprint:ina219_sot23" H 4700 3750 60  0001 C CNN
F 3 "" H 4700 3750 60  0000 C CNN
	1    4900 3750
	1    0    0    -1  
$EndComp
$Comp
L R R2
U 1 1 57B867C2
P 4000 3450
F 0 "R2" V 4080 3450 50  0000 C CNN
F 1 "R" V 4000 3450 50  0000 C CNN
F 2 "Resistors_SMD:R_2512_HandSoldering" V 3930 3450 50  0001 C CNN
F 3 "" H 4000 3450 50  0000 C CNN
	1    4000 3450
	1    0    0    -1  
$EndComp
Text HLabel 5450 3700 2    60   Input ~ 0
SCL
Text HLabel 5450 3600 2    60   Input ~ 0
SDA
Text HLabel 3900 3250 0    60   Input ~ 0
IN+
Text HLabel 3900 3650 0    60   Input ~ 0
IN-
Wire Wire Line
	3900 3650 4200 3650
Wire Wire Line
	4000 3650 4000 3600
Wire Wire Line
	4200 3650 4200 3500
Wire Wire Line
	4200 3500 4550 3500
Connection ~ 4000 3650
Wire Wire Line
	4550 3400 4200 3400
Wire Wire Line
	4200 3400 4200 3250
Wire Wire Line
	4200 3250 3900 3250
Wire Wire Line
	4000 3300 4000 3250
Connection ~ 4000 3250
Wire Wire Line
	4400 3600 4550 3600
Wire Wire Line
	4550 3700 4450 3700
Wire Wire Line
	5450 3600 5350 3600
Wire Wire Line
	5450 3700 5350 3700
$Comp
L +3.3V #PWR038
U 1 1 57B867C4
P 4050 3850
F 0 "#PWR038" H 4050 3700 50  0001 C CNN
F 1 "+3.3V" H 4050 3990 50  0000 C CNN
F 2 "" H 4050 3850 50  0000 C CNN
F 3 "" H 4050 3850 50  0000 C CNN
	1    4050 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 3600 4400 4150
$Comp
L GNDD #PWR039
U 1 1 57B867C5
P 4050 4150
F 0 "#PWR039" H 4050 3900 50  0001 C CNN
F 1 "GNDD" H 4050 4000 50  0000 C CNN
F 2 "" H 4050 4150 50  0000 C CNN
F 3 "" H 4050 4150 50  0000 C CNN
	1    4050 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4450 3700 4450 3850
$Comp
L +3.3V #PWR040
U 1 1 57B867C8
P 5600 3250
F 0 "#PWR040" H 5600 3100 50  0001 C CNN
F 1 "+3.3V" H 5600 3390 50  0000 C CNN
F 2 "" H 5600 3250 50  0000 C CNN
F 3 "" H 5600 3250 50  0000 C CNN
	1    5600 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 3400 5600 3400
Wire Wire Line
	5600 3250 5600 3500
Wire Wire Line
	5600 3500 5350 3500
Connection ~ 5600 3400
$Comp
L C_Small C4
U 1 1 57B867CA
P 4050 4000
F 0 "C4" H 4060 4070 50  0000 L CNN
F 1 "0.1uF" H 4060 3920 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206" H 4050 4000 50  0001 C CNN
F 3 "" H 4050 4000 50  0000 C CNN
	1    4050 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4050 3850 4050 3900
Wire Wire Line
	4450 3850 4050 3850
Wire Wire Line
	4400 4150 4050 4150
Wire Wire Line
	4050 4150 4050 4100
$EndSCHEMATC
