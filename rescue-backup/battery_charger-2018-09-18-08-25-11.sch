EESchema Schematic File Version 2
LIBS:Top_Battery_Plate-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:Sphere_lib
LIBS:Top_Battery_Plate-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MAX745 U11
U 1 1 5A8DA65A
P 5550 3450
F 0 "U11" H 5450 4100 60  0000 C CNN
F 1 "MAX745" H 5550 2900 60  0000 C CNN
F 2 "Sphere_lib_footprint:SSOP-20" H 5550 3450 60  0001 C CNN
F 3 "" H 5550 3450 60  0000 C CNN
F 4 "" H 5550 3450 60  0001 C CNN "FarnellLink"
F 5 "" H 5550 3450 60  0001 C CNN "DigiKeyLink"
F 6 "" H 5550 3450 60  0001 C CNN "MouserLink"
F 7 "U31" H 5550 3450 60  0001 C CNN "InternalName"
	1    5550 3450
	1    0    0    -1  
$EndComp
$Comp
L GNDPWR #PWR049
U 1 1 5A8DA6F7
P 5750 4350
F 0 "#PWR049" H 5750 4150 50  0001 C CNN
F 1 "GNDPWR" H 5750 4220 50  0000 C CNN
F 2 "" H 5750 4300 50  0000 C CNN
F 3 "" H 5750 4300 50  0000 C CNN
	1    5750 4350
	1    0    0    -1  
$EndComp
$Comp
L C_Small C21
U 1 1 5A8DA7A1
P 6450 2700
F 0 "C21" V 6350 2650 50  0000 L CNN
F 1 "0.1uF" V 6550 2600 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 6450 2700 50  0001 C CNN
F 3 "" H 6450 2700 50  0000 C CNN
F 4 "" H 6450 2700 50  0001 C CNN "FarnellLink"
F 5 "" H 6450 2700 50  0001 C CNN "DigiKeyLink"
F 6 "" H 6450 2700 50  0001 C CNN "MouserLink"
F 7 "C12" H 6450 2700 50  0001 C CNN "InternalName"
	1    6450 2700
	0    1    1    0   
$EndComp
$Comp
L CP1_Small C19
U 1 1 5A8DA80D
P 5700 1350
F 0 "C19" H 5710 1420 50  0000 L CNN
F 1 "150uF" H 5710 1270 50  0000 L CNN
F 2 "Capacitors_SMD:CP_Elec_8x10" H 5700 1350 50  0001 C CNN
F 3 "" H 5700 1350 50  0000 C CNN
F 4 "" H 5700 1350 50  0001 C CNN "FarnellLink"
F 5 "" H 5700 1350 50  0001 C CNN "DigiKeyLink"
F 6 "" H 5700 1350 50  0001 C CNN "MouserLink"
F 7 "C27" H 5700 1350 50  0001 C CNN "InternalName"
	1    5700 1350
	1    0    0    -1  
$EndComp
$Comp
L R R22
U 1 1 5A8DA88C
P 5550 4250
F 0 "R22" V 5630 4250 50  0000 C CNN
F 1 "0" V 5550 4250 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 5480 4250 50  0001 C CNN
F 3 "" H 5550 4250 50  0000 C CNN
F 4 "" H 5550 4250 50  0001 C CNN "FarnellLink"
F 5 "" H 5550 4250 50  0001 C CNN "DigiKeyLink"
F 6 "" H 5550 4250 50  0001 C CNN "MouserLink"
F 7 "R33" H 5550 4250 50  0001 C CNN "InternalName"
	1    5550 4250
	0    1    1    0   
$EndComp
$Comp
L D_Schottky_Small D6
U 1 1 5A8DA8E9
P 5450 1150
F 0 "D6" H 5400 1230 50  0000 L CNN
F 1 "D_Schottky_Small" H 5170 1070 50  0001 L CNN
F 2 "Sphere_lib_footprint:SOD-128" V 5450 1150 50  0001 C CNN
F 3 "" V 5450 1150 50  0000 C CNN
F 4 "" V 5450 1150 50  0001 C CNN "FarnellLink"
F 5 "" V 5450 1150 50  0001 C CNN "DigiKeyLink"
F 6 "" V 5450 1150 50  0001 C CNN "MouserLink"
F 7 "D18" V 5450 1150 50  0001 C CNN "InternalName"
	1    5450 1150
	-1   0    0    1   
$EndComp
$Comp
L CP1_Small C15
U 1 1 5A8DAE92
P 3650 3300
F 0 "C15" H 3660 3370 50  0000 L CNN
F 1 "4.7uF" H 3660 3220 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 3650 3300 50  0001 C CNN
F 3 "" H 3650 3300 50  0000 C CNN
F 4 "" H 3650 3300 50  0001 C CNN "FarnellLink"
F 5 "" H 3650 3300 50  0001 C CNN "DigiKeyLink"
F 6 "" H 3650 3300 50  0001 C CNN "MouserLink"
F 7 "C19" H 3650 3300 50  0001 C CNN "InternalName"
	1    3650 3300
	1    0    0    -1  
$EndComp
$Comp
L CP1_Small C14
U 1 1 5A8DAEFA
P 3400 3300
F 0 "C14" H 3410 3370 50  0000 L CNN
F 1 "0.1uF" H 3410 3220 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 3400 3300 50  0001 C CNN
F 3 "" H 3400 3300 50  0000 C CNN
F 4 "" H 3400 3300 50  0001 C CNN "FarnellLink"
F 5 "" H 3400 3300 50  0001 C CNN "DigiKeyLink"
F 6 "" H 3400 3300 50  0001 C CNN "MouserLink"
F 7 "C12" H 3400 3300 50  0001 C CNN "InternalName"
	1    3400 3300
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR050
U 1 1 5A8DB49E
P 3400 3500
F 0 "#PWR050" H 3400 3250 50  0001 C CNN
F 1 "GND" H 3400 3350 50  0000 C CNN
F 2 "" H 3400 3500 50  0000 C CNN
F 3 "" H 3400 3500 50  0000 C CNN
	1    3400 3500
	1    0    0    -1  
$EndComp
$Comp
L CP1_Small C13
U 1 1 5A8DB6DB
P 3150 3300
F 0 "C13" H 3160 3370 50  0000 L CNN
F 1 "0.1uF" H 3160 3220 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 3150 3300 50  0001 C CNN
F 3 "" H 3150 3300 50  0000 C CNN
F 4 "" H 3150 3300 50  0001 C CNN "FarnellLink"
F 5 "" H 3150 3300 50  0001 C CNN "DigiKeyLink"
F 6 "" H 3150 3300 50  0001 C CNN "MouserLink"
F 7 "C12" H 3150 3300 50  0001 C CNN "InternalName"
	1    3150 3300
	1    0    0    -1  
$EndComp
$Comp
L +24V #PWR051
U 1 1 5A8DBABC
P 3150 2850
F 0 "#PWR051" H 3150 2700 50  0001 C CNN
F 1 "+24V" H 3150 2990 50  0000 C CNN
F 2 "" H 3150 2850 50  0000 C CNN
F 3 "" H 3150 2850 50  0000 C CNN
	1    3150 2850
	1    0    0    -1  
$EndComp
$Comp
L R_Small R18
U 1 1 5A8DBF68
P 4450 3250
F 0 "R18" V 4400 3050 50  0000 L CNN
F 1 "10k" V 4400 3300 50  0000 L CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" H 4450 3250 50  0001 C CNN
F 3 "" H 4450 3250 50  0000 C CNN
F 4 "" H 4450 3250 50  0001 C CNN "FarnellLink"
F 5 "" H 4450 3250 50  0001 C CNN "DigiKeyLink"
F 6 "" H 4450 3250 50  0001 C CNN "MouserLink"
F 7 "" H 4450 3250 50  0001 C CNN "InternalName"
	1    4450 3250
	0    1    1    0   
$EndComp
$Comp
L C_Small C18
U 1 1 5A8DC569
P 4700 3350
F 0 "C18" V 4650 3200 50  0000 L CNN
F 1 "47nF" V 4650 3400 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 4700 3350 50  0001 C CNN
F 3 "" H 4700 3350 50  0000 C CNN
F 4 "" H 4700 3350 50  0001 C CNN "FarnellLink"
F 5 "" H 4700 3350 50  0001 C CNN "DigiKeyLink"
F 6 "" H 4700 3350 50  0001 C CNN "MouserLink"
F 7 "C17" H 4700 3350 50  0001 C CNN "InternalName"
	1    4700 3350
	0    1    1    0   
$EndComp
$Comp
L C_Small C17
U 1 1 5A8DCC2C
P 4250 3350
F 0 "C17" V 4200 3200 50  0000 L CNN
F 1 "0.1uF" V 4200 3400 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 4250 3350 50  0001 C CNN
F 3 "" H 4250 3350 50  0000 C CNN
F 4 "" H 4250 3350 50  0001 C CNN "FarnellLink"
F 5 "" H 4250 3350 50  0001 C CNN "DigiKeyLink"
F 6 "" H 4250 3350 50  0001 C CNN "MouserLink"
F 7 "C12" H 4250 3350 50  0001 C CNN "InternalName"
	1    4250 3350
	0    1    1    0   
$EndComp
$Comp
L C_Small C16
U 1 1 5A8DE591
P 4100 3950
F 0 "C16" H 3900 4000 50  0000 L CNN
F 1 "0.22uF" H 3800 3850 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 4100 3950 50  0001 C CNN
F 3 "" H 4100 3950 50  0000 C CNN
F 4 "" H 4100 3950 50  0001 C CNN "FarnellLink"
F 5 "" H 4100 3950 50  0001 C CNN "DigiKeyLink"
F 6 "" H 4100 3950 50  0001 C CNN "MouserLink"
F 7 "C9" H 4100 3950 50  0001 C CNN "InternalName"
	1    4100 3950
	1    0    0    -1  
$EndComp
Text Label 3400 3150 0    60   ~ 0
VL
Text Label 6350 3750 2    60   ~ 0
VL
$Comp
L LED D8
U 1 1 5A8DF267
P 6500 4350
F 0 "D8" H 6500 4450 50  0000 C CNN
F 1 "LED" H 6500 4250 50  0000 C CNN
F 2 "LEDs:LED_0603_HandSoldering" H 6500 4350 50  0001 C CNN
F 3 "" H 6500 4350 50  0000 C CNN
F 4 "" H 6500 4350 50  0001 C CNN "FarnellLink"
F 5 "" H 6500 4350 50  0001 C CNN "DigiKeyLink"
F 6 "" H 6500 4350 50  0001 C CNN "MouserLink"
F 7 "D16" H 6500 4350 50  0001 C CNN "InternalName"
	1    6500 4350
	0    1    1    0   
$EndComp
$Comp
L R_Small R24
U 1 1 5A8DF3BF
P 6500 3950
F 0 "R24" V 6450 3750 50  0000 L CNN
F 1 "1k" V 6450 3900 50  0000 L CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" H 6500 3950 50  0001 C CNN
F 3 "" H 6500 3950 50  0000 C CNN
F 4 "" H 6500 3950 50  0001 C CNN "FarnellLink"
F 5 "" H 6500 3950 50  0001 C CNN "DigiKeyLink"
F 6 "" H 6500 3950 50  0001 C CNN "MouserLink"
F 7 "R26" H 6500 3950 50  0001 C CNN "InternalName"
	1    6500 3950
	-1   0    0    1   
$EndComp
Text Label 6500 4700 0    60   ~ 0
VL
$Comp
L D_Small D7
U 1 1 5A8E0332
P 6100 2700
F 0 "D7" H 6050 2780 50  0000 L CNN
F 1 "D_Small" H 5950 2620 50  0001 L CNN
F 2 "Diodes_SMD:D_SOD-123" V 6100 2700 50  0001 C CNN
F 3 "" V 6100 2700 50  0000 C CNN
F 4 "" V 6100 2700 50  0001 C CNN "FarnellLink"
F 5 "" V 6100 2700 50  0001 C CNN "DigiKeyLink"
F 6 "" V 6100 2700 50  0001 C CNN "MouserLink"
F 7 "D17" V 6100 2700 50  0001 C CNN "InternalName"
	1    6100 2700
	-1   0    0    1   
$EndComp
Wire Wire Line
	3400 3400 3400 3500
Wire Wire Line
	3150 3450 4500 3450
Wire Wire Line
	3650 3450 3650 3400
Connection ~ 3400 3450
Wire Wire Line
	3400 3150 4900 3150
Wire Wire Line
	3400 3150 3400 3200
Wire Wire Line
	3650 3200 3650 3150
Connection ~ 3650 3150
Wire Wire Line
	3150 2850 3150 3200
Wire Wire Line
	3150 3050 4900 3050
Wire Wire Line
	3150 3400 3150 3450
Connection ~ 3150 3050
Wire Wire Line
	4900 3350 4800 3350
Wire Wire Line
	4550 3250 4900 3250
Wire Wire Line
	4350 3250 4100 3250
Wire Wire Line
	4100 3250 4100 3350
Wire Wire Line
	4100 3350 4150 3350
Wire Wire Line
	4500 3450 4500 3350
Wire Wire Line
	4350 3350 4600 3350
Connection ~ 3650 3450
Connection ~ 4500 3350
Wire Wire Line
	3650 3550 4900 3550
Wire Wire Line
	4900 3450 4650 3450
Wire Wire Line
	4650 3450 4650 3500
Wire Wire Line
	4650 3500 3550 3500
Wire Wire Line
	4900 3850 4850 3850
Wire Wire Line
	4850 3850 4850 4000
Wire Wire Line
	6200 3850 6250 3850
Wire Wire Line
	6250 3850 6250 3950
Wire Wire Line
	6200 3750 6350 3750
Wire Wire Line
	6500 4050 6500 4150
Wire Wire Line
	6500 4550 6500 4700
Wire Wire Line
	6550 3050 6550 2700
Wire Wire Line
	6200 2950 6300 2950
Wire Wire Line
	6300 2950 6300 2700
Wire Wire Line
	6200 2700 6350 2700
Connection ~ 6300 2700
$Comp
L R_Small R23
U 1 1 5A8E0AAD
P 5750 2700
F 0 "R23" V 5700 2500 50  0000 L CNN
F 1 "24" V 5700 2650 50  0000 L CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" H 5750 2700 50  0001 C CNN
F 3 "" H 5750 2700 50  0000 C CNN
F 4 "" H 5750 2700 50  0001 C CNN "FarnellLink"
F 5 "" H 5750 2700 50  0001 C CNN "DigiKeyLink"
F 6 "" H 5750 2700 50  0001 C CNN "MouserLink"
F 7 "" H 5750 2700 50  0001 C CNN "InternalName"
	1    5750 2700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5850 2700 6000 2700
Wire Wire Line
	5650 2700 5500 2700
Text Label 5500 2700 0    60   ~ 0
VL
$Comp
L CP1_Small C20
U 1 1 5A8E180E
P 6000 1350
F 0 "C20" H 6010 1420 50  0000 L CNN
F 1 "150uF" H 6010 1270 50  0000 L CNN
F 2 "Capacitors_SMD:CP_Elec_8x10" H 6000 1350 50  0001 C CNN
F 3 "" H 6000 1350 50  0000 C CNN
F 4 "" H 6000 1350 50  0001 C CNN "FarnellLink"
F 5 "" H 6000 1350 50  0001 C CNN "DigiKeyLink"
F 6 "" H 6000 1350 50  0001 C CNN "MouserLink"
F 7 "C27" H 6000 1350 50  0001 C CNN "InternalName"
	1    6000 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 1150 5350 1150
Wire Wire Line
	5550 1150 6300 1150
Wire Wire Line
	6000 1250 6000 1150
Connection ~ 6000 1150
Wire Wire Line
	5700 1250 5700 1150
Connection ~ 5700 1150
Wire Wire Line
	5700 1450 5700 1500
Wire Wire Line
	6000 1500 6000 1450
$Comp
L +24V #PWR052
U 1 1 5A8E2CBD
P 6300 1100
F 0 "#PWR052" H 6300 950 50  0001 C CNN
F 1 "+24V" H 6300 1240 50  0000 C CNN
F 2 "" H 6300 1100 50  0000 C CNN
F 3 "" H 6300 1100 50  0000 C CNN
	1    6300 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6300 1150 6300 1100
$Comp
L GNDPWR #PWR053
U 1 1 5A8E4705
P 8850 3650
F 0 "#PWR053" H 8850 3450 50  0001 C CNN
F 1 "GNDPWR" H 8850 3520 50  0000 C CNN
F 2 "" H 8850 3600 50  0000 C CNN
F 3 "" H 8850 3600 50  0000 C CNN
	1    8850 3650
	1    0    0    -1  
$EndComp
$Comp
L CP1_Small C23
U 1 1 5A8E4749
P 8850 3350
F 0 "C23" H 8860 3420 50  0000 L CNN
F 1 "68uF" H 8860 3270 50  0000 L CNN
F 2 "Capacitors_SMD:C_1210_HandSoldering" H 8850 3350 50  0001 C CNN
F 3 "" H 8850 3350 50  0000 C CNN
F 4 "" H 8850 3350 50  0001 C CNN "FarnellLink"
F 5 "" H 8850 3350 50  0001 C CNN "DigiKeyLink"
F 6 "" H 8850 3350 50  0001 C CNN "MouserLink"
F 7 "C29" H 8850 3350 50  0001 C CNN "InternalName"
	1    8850 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	8850 3600 9500 3600
Wire Wire Line
	8850 3450 8850 3650
Connection ~ 8850 3600
Wire Wire Line
	8850 3250 8850 3200
Wire Wire Line
	8600 3200 9500 3200
$Comp
L R_Small R28
U 1 1 5A8E4B78
P 8500 3200
F 0 "R28" V 8700 3150 50  0000 L CNN
F 1 "0.04 1%" V 8600 3050 50  0000 L CNN
F 2 "Resistors_SMD:R_2512" H 8500 3200 50  0001 C CNN
F 3 "" H 8500 3200 50  0000 C CNN
F 4 "" H 8500 3200 50  0001 C CNN "FarnellLink"
F 5 "" H 8500 3200 50  0001 C CNN "DigiKeyLink"
F 6 "" H 8500 3200 50  0001 C CNN "MouserLink"
F 7 "R42" H 8500 3200 50  0001 C CNN "InternalName"
	1    8500 3200
	0    -1   -1   0   
$EndComp
$Comp
L C_Small C22
U 1 1 5A8E4DAB
P 8500 3350
F 0 "C22" V 8450 3200 50  0000 L CNN
F 1 "1nF" V 8450 3400 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 8500 3350 50  0001 C CNN
F 3 "" H 8500 3350 50  0000 C CNN
F 4 "" H 8500 3350 50  0001 C CNN "FarnellLink"
F 5 "" H 8500 3350 50  0001 C CNN "DigiKeyLink"
F 6 "" H 8500 3350 50  0001 C CNN "MouserLink"
F 7 "C25" H 8500 3350 50  0001 C CNN "InternalName"
	1    8500 3350
	0    1    1    0   
$EndComp
Wire Wire Line
	8200 3200 8400 3200
Connection ~ 8850 3200
Wire Wire Line
	8650 4150 8650 3200
Wire Wire Line
	8650 3350 8600 3350
Connection ~ 8650 3200
Connection ~ 8650 3350
Wire Wire Line
	8300 4100 8300 3200
Wire Wire Line
	8400 3350 8300 3350
Connection ~ 8300 3350
$Comp
L D_Schottky_Small D10
U 1 1 5A8E599D
P 8100 3200
F 0 "D10" H 8050 3280 50  0000 L CNN
F 1 "D_Schottky_Small" H 7820 3120 50  0001 L CNN
F 2 "Sphere_lib_footprint:SOD-128" V 8100 3200 50  0001 C CNN
F 3 "" V 8100 3200 50  0000 C CNN
F 4 "" V 8100 3200 50  0001 C CNN "FarnellLink"
F 5 "" V 8100 3200 50  0001 C CNN "DigiKeyLink"
F 6 "" V 8100 3200 50  0001 C CNN "MouserLink"
F 7 "D18" V 8100 3200 50  0001 C CNN "InternalName"
	1    8100 3200
	-1   0    0    1   
$EndComp
Wire Wire Line
	6600 4150 8650 4150
Wire Wire Line
	6650 4100 8300 4100
$Comp
L INDUCTOR_SMALL L1
U 1 1 5A8E5F51
P 7650 3200
F 0 "L1" H 7650 3300 50  0000 C CNN
F 1 "22uH" H 7650 3150 50  0000 C CNN
F 2 "Sphere_lib_footprint:SDR1307A_Bournes_Inductor" H 7650 3200 50  0001 C CNN
F 3 "" H 7650 3200 50  0000 C CNN
F 4 "" H 7650 3200 50  0001 C CNN "FarnellLink"
F 5 "" H 7650 3200 50  0001 C CNN "DigiKeyLink"
F 6 "" H 7650 3200 50  0001 C CNN "MouserLink"
F 7 "L16" H 7650 3200 50  0001 C CNN "InternalName"
	1    7650 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	7900 3200 8000 3200
Connection ~ 8300 3200
$Comp
L D_Schottky_Small D9
U 1 1 5A8E613C
P 7350 3500
F 0 "D9" H 7300 3580 50  0000 L CNN
F 1 "D_Schottky_Small" H 7070 3420 50  0001 L CNN
F 2 "Sphere_lib_footprint:SOD-128" V 7350 3500 50  0001 C CNN
F 3 "" V 7350 3500 50  0000 C CNN
F 4 "" V 7350 3500 50  0001 C CNN "FarnellLink"
F 5 "" V 7350 3500 50  0001 C CNN "DigiKeyLink"
F 6 "" V 7350 3500 50  0001 C CNN "MouserLink"
F 7 "D18" V 7350 3500 50  0001 C CNN "InternalName"
	1    7350 3500
	0    1    1    0   
$EndComp
$Comp
L GNDPWR #PWR054
U 1 1 5A8E61F6
P 7350 3850
F 0 "#PWR054" H 7350 3650 50  0001 C CNN
F 1 "GNDPWR" H 7350 3720 50  0000 C CNN
F 2 "" H 7350 3800 50  0000 C CNN
F 3 "" H 7350 3800 50  0000 C CNN
	1    7350 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	7050 3200 7400 3200
Wire Wire Line
	7350 3200 7350 3400
Wire Wire Line
	7350 3600 7350 3850
$Comp
L R_Small R26
U 1 1 5A8E6326
P 6600 3250
F 0 "R26" V 6550 3450 50  0000 L CNN
F 1 "SHORT" V 6550 3150 50  0000 L CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" H 6600 3250 50  0001 C CNN
F 3 "" H 6600 3250 50  0000 C CNN
F 4 "" H 6600 3250 50  0001 C CNN "FarnellLink"
F 5 "" H 6600 3250 50  0001 C CNN "DigiKeyLink"
F 6 "" H 6600 3250 50  0001 C CNN "MouserLink"
F 7 "R33" H 6600 3250 50  0001 C CNN "InternalName"
	1    6600 3250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6700 3800 7350 3800
Connection ~ 7350 3800
Wire Wire Line
	5400 4250 5300 4250
Wire Wire Line
	5300 4250 5300 4350
Wire Wire Line
	5700 4250 5750 4250
Wire Wire Line
	5750 4250 5750 4350
$Comp
L Dual_NMOS_FET_IRF7303 Q2
U 1 1 5A8DB3A2
P 7000 2900
F 0 "Q2" H 7300 2950 50  0000 R CNN
F 1 "Dual_NMOS_FET_IRF7303" H 7500 2600 50  0001 R CNN
F 2 "Sphere_lib_footprint:SO-8" H 7200 3000 50  0001 C CNN
F 3 "" H 7000 2900 50  0000 C CNN
F 4 "" H 7000 2900 50  0001 C CNN "FarnellLink"
F 5 "" H 7000 2900 50  0001 C CNN "DigiKeyLink"
F 6 "" H 7000 2900 50  0001 C CNN "MouserLink"
F 7 "Q7" H 7000 2900 50  0001 C CNN "InternalName"
F 8 "" H 7000 2900 50  0001 C CNN "FarnellLink"
F 9 "" H 7000 2900 50  0001 C CNN "DigiKeyLink"
F 10 "" H 7000 2900 50  0001 C CNN "MouserLink"
F 11 "Q7" H 7000 2900 50  0001 C CNN "InternalName"
	1    7000 2900
	1    0    0    -1  
$EndComp
$Comp
L Dual_NMOS_FET_IRF7303 Q2
U 2 1 5A8DB489
P 7050 3450
F 0 "Q2" H 7350 3500 50  0000 R CNN
F 1 "Dual_NMOS_FET_IRF7303" H 7550 3150 50  0001 R CNN
F 2 "Sphere_lib_footprint:SO-8" H 7250 3550 50  0001 C CNN
F 3 "" H 7050 3450 50  0000 C CNN
F 4 "" H 7050 3450 50  0001 C CNN "FarnellLink"
F 5 "" H 7050 3450 50  0001 C CNN "DigiKeyLink"
F 6 "" H 7050 3450 50  0001 C CNN "MouserLink"
F 7 "Q7" H 7050 3450 50  0001 C CNN "InternalName"
F 8 "" H 7050 3450 50  0001 C CNN "FarnellLink"
F 9 "" H 7050 3450 50  0001 C CNN "DigiKeyLink"
F 10 "" H 7050 3450 50  0001 C CNN "MouserLink"
F 11 "Q7" H 7050 3450 50  0001 C CNN "InternalName"
	2    7050 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 3850 6500 3650
Wire Wire Line
	6500 3650 6200 3650
Wire Wire Line
	6200 3550 6600 3550
Wire Wire Line
	6600 3550 6600 4150
Wire Wire Line
	6650 4100 6650 3450
Connection ~ 6850 4100
Wire Wire Line
	6650 3450 6200 3450
Wire Wire Line
	6700 3350 6700 3800
Wire Wire Line
	6700 3350 6200 3350
Wire Wire Line
	7150 3650 7150 3800
Connection ~ 7150 3800
Wire Wire Line
	7250 3250 7250 3200
Connection ~ 7350 3200
Connection ~ 7250 3200
Wire Wire Line
	6750 3450 6850 3450
Connection ~ 7150 3200
$Comp
L R_Small R25
U 1 1 5A8DD30A
P 6600 3150
F 0 "R25" V 6550 3350 50  0000 L CNN
F 1 "SHORT" V 6550 3050 50  0000 L CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" H 6600 3150 50  0001 C CNN
F 3 "" H 6600 3150 50  0000 C CNN
F 4 "" H 6600 3150 50  0001 C CNN "FarnellLink"
F 5 "" H 6600 3150 50  0001 C CNN "DigiKeyLink"
F 6 "" H 6600 3150 50  0001 C CNN "MouserLink"
F 7 "R33" H 6600 3150 50  0001 C CNN "InternalName"
	1    6600 3150
	0    -1   -1   0   
$EndComp
$Comp
L R_Small R27
U 1 1 5A8DD401
P 6950 3200
F 0 "R27" V 7050 3200 50  0000 L CNN
F 1 "SHORT" V 6900 3100 50  0000 L CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" H 6950 3200 50  0001 C CNN
F 3 "" H 6950 3200 50  0000 C CNN
F 4 "" H 6950 3200 50  0001 C CNN "FarnellLink"
F 5 "" H 6950 3200 50  0001 C CNN "DigiKeyLink"
F 6 "" H 6950 3200 50  0001 C CNN "MouserLink"
F 7 "R33" H 6950 3200 50  0001 C CNN "InternalName"
	1    6950 3200
	0    -1   -1   0   
$EndComp
Connection ~ 6550 3050
Wire Wire Line
	6700 3250 6750 3250
Wire Wire Line
	6750 3250 6750 3450
Wire Wire Line
	6700 3150 6750 3150
Wire Wire Line
	6750 3150 6750 2900
Wire Wire Line
	6200 3050 6800 3050
Wire Wire Line
	6800 3050 6800 3200
Wire Wire Line
	6800 3200 6850 3200
Wire Wire Line
	6750 2900 6800 2900
Wire Wire Line
	7150 3250 7150 3200
Wire Wire Line
	7100 3100 7100 3200
Connection ~ 7100 3200
Wire Wire Line
	6500 3150 6200 3150
Wire Wire Line
	6200 3250 6500 3250
Wire Wire Line
	7100 2700 7100 2600
Wire Wire Line
	7100 2600 7200 2600
Wire Wire Line
	7200 2500 7200 2700
Connection ~ 7200 2600
$Comp
L +24V #PWR055
U 1 1 5A8DF1B5
P 7200 2500
F 0 "#PWR055" H 7200 2350 50  0001 C CNN
F 1 "+24V" H 7200 2640 50  0000 C CNN
F 2 "" H 7200 2500 50  0000 C CNN
F 3 "" H 7200 2500 50  0000 C CNN
	1    7200 2500
	1    0    0    -1  
$EndComp
$Comp
L R_Small R16
U 1 1 5A8F41B6
P 4300 3750
F 0 "R16" V 4400 3700 50  0000 L CNN
F 1 "100k" V 4250 3700 50  0000 L CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" H 4300 3750 50  0001 C CNN
F 3 "" H 4300 3750 50  0000 C CNN
F 4 "" H 4300 3750 50  0001 C CNN "FarnellLink"
F 5 "" H 4300 3750 50  0001 C CNN "DigiKeyLink"
F 6 "" H 4300 3750 50  0001 C CNN "MouserLink"
F 7 "" H 4300 3750 50  0001 C CNN "InternalName"
	1    4300 3750
	-1   0    0    1   
$EndComp
Wire Wire Line
	4100 3550 4100 3850
$Comp
L R_Small R20
U 1 1 5A8F460D
P 4500 3750
F 0 "R20" V 4450 3700 50  0000 L CNN
F 1 "15k" V 4350 3700 50  0000 L CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" H 4500 3750 50  0001 C CNN
F 3 "" H 4500 3750 50  0000 C CNN
F 4 "" H 4500 3750 50  0001 C CNN "FarnellLink"
F 5 "" H 4500 3750 50  0001 C CNN "DigiKeyLink"
F 6 "" H 4500 3750 50  0001 C CNN "MouserLink"
F 7 "R41" H 4500 3750 50  0001 C CNN "InternalName"
	1    4500 3750
	-1   0    0    1   
$EndComp
$Comp
L R_Small R17
U 1 1 5A8F4685
P 4300 4200
F 0 "R17" V 4400 4100 50  0000 L CNN
F 1 "100k" V 4250 4100 50  0000 L CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" H 4300 4200 50  0001 C CNN
F 3 "" H 4300 4200 50  0000 C CNN
F 4 "" H 4300 4200 50  0001 C CNN "FarnellLink"
F 5 "" H 4300 4200 50  0001 C CNN "DigiKeyLink"
F 6 "" H 4300 4200 50  0001 C CNN "MouserLink"
F 7 "R7" H 4300 4200 50  0001 C CNN "InternalName"
	1    4300 4200
	-1   0    0    1   
$EndComp
$Comp
L R_Small R21
U 1 1 5A8F4718
P 4500 4200
F 0 "R21" V 4450 4100 50  0000 L CNN
F 1 "100k" V 4350 4100 50  0000 L CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" H 4500 4200 50  0001 C CNN
F 3 "" H 4500 4200 50  0000 C CNN
F 4 "" H 4500 4200 50  0001 C CNN "FarnellLink"
F 5 "" H 4500 4200 50  0001 C CNN "DigiKeyLink"
F 6 "" H 4500 4200 50  0001 C CNN "MouserLink"
F 7 "R7" H 4500 4200 50  0001 C CNN "InternalName"
	1    4500 4200
	-1   0    0    1   
$EndComp
Wire Wire Line
	4300 4300 4300 4350
Wire Wire Line
	3950 4350 4500 4350
Wire Wire Line
	4500 4350 4500 4300
Wire Wire Line
	4400 4350 4400 4400
Connection ~ 4400 4350
Wire Wire Line
	4300 3650 4300 3550
Connection ~ 4300 3550
Wire Wire Line
	4500 3650 4500 3550
Connection ~ 4500 3550
Wire Wire Line
	4900 3650 4200 3650
Wire Wire Line
	4200 3650 4200 3950
Wire Wire Line
	4200 3950 4300 3950
Wire Wire Line
	4300 3850 4300 4100
Connection ~ 4300 3950
Wire Wire Line
	4500 4100 4500 3850
Wire Wire Line
	4500 3950 4700 3950
Wire Wire Line
	4700 3950 4700 3750
Wire Wire Line
	4700 3750 4900 3750
Connection ~ 4500 3950
$Comp
L R_Small R15
U 1 1 5A8F6DC5
P 3650 3750
F 0 "R15" V 3600 3600 50  0000 L CNN
F 1 "100k" V 3600 3750 50  0000 L CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" H 3650 3750 50  0001 C CNN
F 3 "" H 3650 3750 50  0000 C CNN
F 4 "" H 3650 3750 50  0001 C CNN "FarnellLink"
F 5 "" H 3650 3750 50  0001 C CNN "DigiKeyLink"
F 6 "" H 3650 3750 50  0001 C CNN "MouserLink"
F 7 "R7" H 3650 3750 50  0001 C CNN "InternalName"
	1    3650 3750
	-1   0    0    1   
$EndComp
Wire Wire Line
	3650 3550 3650 3650
Connection ~ 4100 3550
Wire Wire Line
	3550 3500 3550 3900
Wire Wire Line
	3550 3900 3650 3900
Wire Wire Line
	3650 3850 3650 4000
$Comp
L CONN_01X02 P13
U 1 1 5A8F751D
P 3400 4050
F 0 "P13" H 3400 4200 50  0000 C CNN
F 1 "CONN_01X02" V 3500 4050 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 3400 4050 50  0001 C CNN
F 3 "" H 3400 4050 50  0000 C CNN
F 4 "" H 3400 4050 50  0001 C CNN "FarnellLink"
F 5 "" H 3400 4050 50  0001 C CNN "DigiKeyLink"
F 6 "" H 3400 4050 50  0001 C CNN "MouserLink"
F 7 "" H 3400 4050 50  0001 C CNN "InternalName"
	1    3400 4050
	-1   0    0    1   
$EndComp
Wire Wire Line
	3650 4000 3600 4000
Connection ~ 3650 3900
Wire Wire Line
	3600 4100 3950 4100
Wire Wire Line
	3950 4100 3950 4350
Connection ~ 4300 4350
Wire Wire Line
	4100 4050 4100 4350
Connection ~ 4100 4350
$Comp
L R_Small R19
U 1 1 5A8F849C
P 4500 2950
F 0 "R19" V 4450 2750 50  0000 L CNN
F 1 "8k" V 4450 2950 50  0000 L CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" H 4500 2950 50  0001 C CNN
F 3 "" H 4500 2950 50  0000 C CNN
F 4 "" H 4500 2950 50  0001 C CNN "FarnellLink"
F 5 "" H 4500 2950 50  0001 C CNN "DigiKeyLink"
F 6 "" H 4500 2950 50  0001 C CNN "MouserLink"
F 7 "" H 4500 2950 50  0001 C CNN "InternalName"
	1    4500 2950
	0    -1   -1   0   
$EndComp
$Comp
L TEST_1P W2
U 1 1 5A8F8566
P 4750 2900
F 0 "W2" H 4750 3170 50  0000 C CNN
F 1 "TEST_1P" H 4750 3100 50  0000 C CNN
F 2 "Sphere_lib_footprint:TestPoint" H 4950 2900 50  0001 C CNN
F 3 "" H 4950 2900 50  0000 C CNN
F 4 "" H 4950 2900 50  0001 C CNN "FarnellLink"
F 5 "" H 4950 2900 50  0001 C CNN "DigiKeyLink"
F 6 "" H 4950 2900 50  0001 C CNN "MouserLink"
F 7 "PCB-1" H 4950 2900 50  0001 C CNN "InternalName"
	1    4750 2900
	1    0    0    -1  
$EndComp
$Comp
L TEST_1P W1
U 1 1 5A8F879B
P 4250 2900
F 0 "W1" H 4250 3170 50  0000 C CNN
F 1 "TEST_1P" H 4250 3100 50  0000 C CNN
F 2 "Sphere_lib_footprint:TestPoint" H 4450 2900 50  0001 C CNN
F 3 "" H 4450 2900 50  0000 C CNN
F 4 "" H 4450 2900 50  0001 C CNN "FarnellLink"
F 5 "" H 4450 2900 50  0001 C CNN "DigiKeyLink"
F 6 "" H 4450 2900 50  0001 C CNN "MouserLink"
F 7 "PCB-1" H 4450 2900 50  0001 C CNN "InternalName"
	1    4250 2900
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR056
U 1 1 5A8F8832
P 3900 2850
F 0 "#PWR056" H 3900 2600 50  0001 C CNN
F 1 "GND" H 3900 2700 50  0000 C CNN
F 2 "" H 3900 2850 50  0000 C CNN
F 3 "" H 3900 2850 50  0000 C CNN
	1    3900 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 2850 3900 2800
Wire Wire Line
	3900 2800 4100 2800
Wire Wire Line
	4100 2800 4100 2950
Wire Wire Line
	4100 2950 4400 2950
Wire Wire Line
	4250 2900 4250 2950
Connection ~ 4250 2950
Wire Wire Line
	4750 2900 4750 2950
Wire Wire Line
	4600 2950 4900 2950
Connection ~ 4750 2950
Text HLabel 9500 3200 2    118  Input ~ 0
BAT
Text HLabel 9500 3600 2    118  Input ~ 0
PWR_GND
Text HLabel 4550 1150 0    118  Input ~ 0
24Vin
$Comp
L GNDPWR #PWR057
U 1 1 5A8E20D3
P 5850 1550
F 0 "#PWR057" H 5850 1350 50  0001 C CNN
F 1 "GNDPWR" H 5850 1420 50  0000 C CNN
F 2 "" H 5850 1500 50  0000 C CNN
F 3 "" H 5850 1500 50  0000 C CNN
	1    5850 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 1500 6000 1500
Wire Wire Line
	5850 1500 5850 1550
Connection ~ 5850 1500
Text HLabel 9650 4050 2    118  Input ~ 0
GND
Wire Wire Line
	9650 4050 9400 4050
Wire Wire Line
	9400 4050 9400 4100
$Comp
L GNDD #PWR058
U 1 1 5B696E3D
P 4400 4400
F 0 "#PWR058" H 4400 4150 50  0001 C CNN
F 1 "GNDD" H 4400 4250 50  0000 C CNN
F 2 "" H 4400 4400 50  0000 C CNN
F 3 "" H 4400 4400 50  0000 C CNN
	1    4400 4400
	1    0    0    -1  
$EndComp
$Comp
L GNDD #PWR059
U 1 1 5B6972FA
P 4850 4000
F 0 "#PWR059" H 4850 3750 50  0001 C CNN
F 1 "GNDD" H 4850 3850 50  0000 C CNN
F 2 "" H 4850 4000 50  0000 C CNN
F 3 "" H 4850 4000 50  0000 C CNN
	1    4850 4000
	1    0    0    -1  
$EndComp
$Comp
L GNDD #PWR060
U 1 1 5B697374
P 5300 4350
F 0 "#PWR060" H 5300 4100 50  0001 C CNN
F 1 "GNDD" H 5300 4200 50  0000 C CNN
F 2 "" H 5300 4350 50  0000 C CNN
F 3 "" H 5300 4350 50  0000 C CNN
	1    5300 4350
	1    0    0    -1  
$EndComp
$Comp
L GNDD #PWR061
U 1 1 5B697AF4
P 6250 3950
F 0 "#PWR061" H 6250 3700 50  0001 C CNN
F 1 "GNDD" H 6250 3800 50  0000 C CNN
F 2 "" H 6250 3950 50  0000 C CNN
F 3 "" H 6250 3950 50  0000 C CNN
	1    6250 3950
	1    0    0    -1  
$EndComp
$Comp
L GNDD #PWR062
U 1 1 5B697C57
P 9400 4100
F 0 "#PWR062" H 9400 3850 50  0001 C CNN
F 1 "GNDD" H 9400 3950 50  0000 C CNN
F 2 "" H 9400 4100 50  0000 C CNN
F 3 "" H 9400 4100 50  0000 C CNN
	1    9400 4100
	1    0    0    -1  
$EndComp
$EndSCHEMATC
