# README #

This repository contains the design files for the Power Management Board for the deep sea sphere. More information about the project can be found [here](). A BOM and pdf version of the schematic can be found inside the /docs folder. 

![Main Schematic](docs/Top_Battery_Plate.png)
![PCB](docs/IMG_0057.JPG)
